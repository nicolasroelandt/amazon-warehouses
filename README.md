# Amazon Warehouses in California

<!-- badges: start -->
<!-- badges: end -->

The goal of the amazonWarehouses package is to analyse Amazon warehouses location worlwide.

Its [first vignette](https://nicolasroelandt.gitlab.io/amazon-warehouses/articles/california.html) is centered on the California area.

The original datasource is [MWPVL international Inc.](https://www.mwpvl.com/html/amazon.html).
We recreate the database from there and geolocalised the warehouses using the [Here.com API](https://developer.here.com/develop/rest-apis).

This package provides some functions to use the [Here.com API](https://developer.here.com/develop/rest-apis) within R.

A more detailed reference of the functions can be found [here](https://nicolasroelandt.gitlab.io/amazon-warehouses/).

## Installation

``` r
remotes::install_gitlab("nicolasroelandt/amazonWarehouses")
```

## Load the package

``` r
library(amazonWarehouses)
```

